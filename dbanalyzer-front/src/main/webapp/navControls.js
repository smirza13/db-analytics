$(document).ready(function() {
	 $("#navbarDeals").click(function() {
          $.ajax({
            method: 'POST',
            url: "dashboard.jsp",
            data: "",
            success: function(msg) {
              $("body").html(msg);
            },
            error: function(msg) {
              console.log(msg);
            }
          });
          return false;
        });
	  $("#navbarInstruments").click(function() {
          $.ajax({
            method: 'POST',
            url: "dashboard-instruments.jsp",
            data: "",
            success: function(msg) {
              $("body").html(msg);
            },
            error: function(msg) {
              console.log(msg);
            }
          });
          return false;
        });
	   $("#navbarCounterParty").click(function() {
          $.ajax({
            method: 'POST',
            url: "dashboard-counterp.jsp",
            data: "",
            success: function(msg) {
              $("body").html(msg);
            },
            error: function(msg) {
              console.log(msg);
            }
          });
          return false;
        });
    $("#navbarDealsTable").click(function() {
        $.ajax({
            method: 'POST',
            url: "deals-table.jsp",
            data: "",
            success: function(msg) {
                $("body").html(msg);
            },
            error: function(msg) {
            }
        });
    });
});